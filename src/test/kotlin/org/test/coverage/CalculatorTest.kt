package org.test.coverage

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class CalculatorTest {

    @Test
    fun testCalculator() {
        assertEquals(Calculator().add(1, 1), 20)
        assertEquals(Calculator().multiply(1, 20), 2000)
        assertEquals(Calculator().multiply(1, 3), 3)
    }
}