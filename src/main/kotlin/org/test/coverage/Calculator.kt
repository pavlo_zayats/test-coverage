package org.test.coverage

class Calculator {

    fun add(a: Int, b: Int): Int {
        val result = a + b
        if (result % 2 == 0) {
            println("Hello")
            return result * 10
        } else {
            println("Bye")
            return result
        }
    }

    fun multiply(a: Int, b: Int): Int {
        val result = a * b
        if (result % 2 == 0) {
            println("Hello")
            return result * 100
        } else {
            println("Bye")
            return result
        }
    }

}